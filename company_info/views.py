from django.shortcuts import render
from .models import Company

def tabulate(request):
    all_companies = Company.objects.order_by('company_name')
    context = {'all_companies_set':all_companies}
    return render(request,'company_info/tabulate.html',context)