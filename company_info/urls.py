from django.conf.urls import url

from . import views
app_name = 'company_info'
urlpatterns = [
    url(r'^$',views.tabulate,name='tabulate')
]