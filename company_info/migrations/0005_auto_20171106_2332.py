# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('company_info', '0004_auto_20171106_1952'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='company_contacts',
        ),
        migrations.AddField(
            model_name='companycontact',
            name='employee_company',
            field=models.ForeignKey(blank=True, null=True, to='company_info.Company'),
        ),
    ]
