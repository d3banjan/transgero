# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='tg_company',
            fields=[
                ('company_id', models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, serialize=False)),
                ('company_name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='tg_company_address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('address_street', models.CharField(max_length=100)),
                ('address_state', models.CharField(max_length=40)),
                ('address_country', models.CharField(max_length=40)),
                ('address_zipcode', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='tg_company_contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('employee_name', models.CharField(max_length=100)),
                ('employee_position', models.CharField(max_length=100)),
                ('employee_phone_number', models.CharField(max_length=15, blank=True, validators=[django.core.validators.RegexValidator(regex='^\\+?1?\\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")])),
            ],
        ),
        migrations.AddField(
            model_name='tg_company',
            name='company_address',
            field=models.OneToOneField(to='company_info.tg_company_address'),
        ),
        migrations.AddField(
            model_name='tg_company',
            name='company_contacts',
            field=models.ForeignKey(to='company_info.tg_company_contact'),
        ),
    ]
