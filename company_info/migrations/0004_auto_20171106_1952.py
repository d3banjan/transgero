# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('company_info', '0003_auto_20171106_1928'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='company_address',
        ),
        migrations.AddField(
            model_name='company',
            name='company_address_country',
            field=models.CharField(max_length=40, blank=True, null=True),
        ),
        migrations.AddField(
            model_name='company',
            name='company_address_state',
            field=models.CharField(max_length=40, blank=True, null=True),
        ),
        migrations.AddField(
            model_name='company',
            name='company_address_street',
            field=models.CharField(max_length=100, blank=True, null=True),
        ),
        migrations.AddField(
            model_name='company',
            name='company_address_zipcode',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='company_name',
            field=models.CharField(max_length=100, blank=True, null=True),
        ),
        migrations.DeleteModel(
            name='CompanyAddress',
        ),
    ]
