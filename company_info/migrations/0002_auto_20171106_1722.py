# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('company_info', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='tg_company',
            new_name='tgCompany',
        ),
        migrations.RenameModel(
            old_name='tg_company_address',
            new_name='tgCompanyAddress',
        ),
        migrations.RenameModel(
            old_name='tg_company_contact',
            new_name='tgCompanyContact',
        ),
    ]
