# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('company_info', '0002_auto_20171106_1722'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='tgCompany',
            new_name='Company',
        ),
        migrations.RenameModel(
            old_name='tgCompanyAddress',
            new_name='CompanyAddress',
        ),
        migrations.RenameModel(
            old_name='tgCompanyContact',
            new_name='CompanyContact',
        ),
    ]
