from django.contrib import admin

from .models import Company,CompanyContact

admin.site.register(Company)
admin.site.register(CompanyContact)
# Register your models here.
