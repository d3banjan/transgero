from django.db import models
import uuid
from django.core.validators import RegexValidator


# Company
class Company(models.Model):
    """
     model for company
    """
    #company_id = models.AutoField(primary_key=True)
    company_id = models.UUIDField(primary_key=True,
                                  default=uuid.uuid4,
                                  editable=False)
    company_name = models.CharField(max_length=100,null=True,blank=True)
    company_address_street = models.CharField(max_length=100,null=True,blank=True)
    company_address_state = models.CharField(max_length=40,null=True,blank=True)
    company_address_country = models.CharField(max_length=40,null=True,blank=True)
    company_address_zipcode = models.PositiveIntegerField(null=True,blank=True)



# Company Contacts
class CompanyContact(models.Model):
    """
     model for company contacts
    """
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'."
                                         " Up to 15 digits allowed.")
    #
    employee_name = models.CharField(max_length=100)
    employee_position = models.CharField(max_length=100)
    # recipe from https://stackoverflow.com/questions/19130942/whats-the-best-way-to-store-phone-number-in-django-models
    employee_phone_number = models.CharField(validators=[phone_regex],
                                    max_length=15,
                                    blank=True)  # validators should be a list
    employee_company = models.ForeignKey(Company,null=True,blank=True)


