# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from django.contrib.auth.forms import ReadOnlyPasswordHashField, AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model, authenticate
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div, HTML
from crispy_forms.bootstrap import StrictButton


class UserCreationForm(forms.ModelForm):

    """ A form for creating new users.

    Includes all the required fields, plus a repeated password.

    """

    error_messages = {
        'duplicate_email': _("A user with that email already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }

    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above, for verification."))

    class Meta:
        model = get_user_model()
        fields = ('email', 'first_name', 'last_name')

    def clean_email(self):
        """ Clean form email.

        :return str email: cleaned email
        :raise forms.ValidationError: Email is duplicated

        """
        # Since User.email is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        email = self.cleaned_data["email"]
        try:
            get_user_model()._default_manager.get(email=email)
        except get_user_model().DoesNotExist:
            return email
        raise forms.ValidationError(
            self.error_messages['duplicate_email'],
            code='duplicate_email',
        )

    def clean_password2(self):
        """ Check that the two password entries match.

        :return str password2: cleaned password2
        :raise forms.ValidationError: password2 != password1

        """
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        """ Save user.

        Save the provided password in hashed format.

        :return custom_user.models.User: user

        """
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):

    """ A form for updating users.

    Includes all the fields on the user, but replaces the password field
    with admin's password hash display field.

    """

    password = ReadOnlyPasswordHashField(label=_("Password"), help_text=_(
        "Raw passwords are not stored, so there is no way to see "
        "this user's password, but you can change the password "
        "using <a href=\"password/\">this form</a>."))

    class Meta:
        model = get_user_model()
        exclude = ()

    def __init__(self, *args, **kwargs):
        """ Init the form."""
        super(UserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        """ Clean password.

        Regardless of what the user provides, return the initial value.
        This is done here, rather than on the field, because the
        field does not have access to the initial value.

        :return str password:

        """
        return self.initial["password"]


class RegistrationForm(UserCreationForm):

    required_css_class = 'required'

    promotion_code = forms.CharField(
            required=False,
            label=_('evtl. Promotion-Code'),
            help_text=_('Do you have any promotion code ?')
    )

    class Meta:
        model = get_user_model()
        fields = ['email', 'gender', 'title', 'first_name', 'last_name',
                  'phone', 'language', 'promotion_code']


class LoginForm(AuthenticationForm):
    """
        Overwritting the django Login form because of several reasons:
            - converts password with sha1 for compatibility reasons with the app
            - takes care of the email format (instead of usernames)
            - checks, if an app user is trying to login
    """

    error_messages = {
        'invalid_login': _(u"E-Mail-Adresse und/oder Ihr Passwort sind nicht korrekt. Es wird zwischen Groß- und Kleinschreibung unterschieden."),
        'inactive': _(u"Ihr Account muss vom Support freigeschaltet werden. Bitte gedulden Sie sich etwas, dies kann bis zu 48 Stunden dauern."),
        'no_cookies': _(u"Your Web browser doesn't appear to have cookies enabled. Cookies are required for logging in."),
        'needs_activation': _(u"Sie müssen zunächst Ihre E-Mail-Adresse verifizieren. Klicken Sie auf den Link in der E-Mail, die wir Ihnen geschickt haben."),
        'no_password': _(u"Sie können sich nicht einloggen. Sie müssen a) den Link in der E-Mail bestätigen und sich dann b) ein Passwort geben. Falls es dabei Probleme gibt, wenden Sie sich bitte an den Support. Wir helfen Ihnen gerne!"),
        'empty_email': _(u"Sie müssen Ihre E-Mail eingeben."),
    }

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.form_class = 'form'
        #self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        layout = self.helper.layout = Layout()
        for field_name, field in self.fields.items():
            layout.append(Field(field_name, placeholder=field.label))
        layout.append(StrictButton('Anmelden', type='submit', css_class='btn btn-default btn-lg btn-block'),)

    def clean(self):

        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        User = get_user_model()

        try:
            user = User.objects.get(email=username)

            if not user.has_usable_password():
                raise forms.ValidationError(
                    self.error_messages['no_password'],
                    code='no_password',
                    params={'username': self.username_field.verbose_name},
                )

        except User.DoesNotExist:
            pass

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache