"""
NOTE: THIS IS SAME AS REGISTRATION/BACKENDS/DEFAULT/URLS

URLconf for registration and activation, using django-registration's
default backend.

If the default behavior of these views is acceptable to you, simply
use a line like this in your root URLconf to set up the default URLs
for registration::

    (r'^accounts/', include('registration.backends.default.urls')),

This will also automatically set up the views in
``django.contrib.auth`` at sensible default locations.

If you'd like to customize registration behavior, feel free to set up
your own URL patterns for these views instead.

"""


from django.conf.urls import include
from django.conf.urls import url
from django.conf import settings
from django.views.generic.base import TemplateView

from .views import ActivationView
from .views import CustomerRegistrationView, EmployeeRegistrationView
from .views import ResendActivationView
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import AuthenticationForm
from customers.views import customer_profile
from employees.views import employee_page


urlpatterns = [
    url(r'^activate/complete/$',
        TemplateView.as_view(template_name='registration/activation_complete.html'),
        name='registration_activation_complete'),
    url(r'^activate/resend/$',
        ResendActivationView.as_view(),
        name='registration_resend_activation'),
    # Activation keys get matched by \w+ instead of the more specific
    # [a-fA-F0-9]{40} because a bad activation key should still get to the view;
    # that way it can return a sensible "invalid key" message instead of a
    # confusing 404.
    url(r'^activate/(?P<activation_key>\w+)/$',
        ActivationView.as_view(),
        name='registration_activate'),
    url(r'^register/complete/$',
        TemplateView.as_view(template_name='registration/registration_complete.html'),
        name='registration_complete'),
    url(r'^register/closed/$',
        TemplateView.as_view(template_name='registration/registration_closed.html'),
        name='registration_disallowed'),

    url(r'^login/$',
       #login_forbidden(auth_views.login),
       auth_views.login,
       {'template_name': 'registration/login.html',
        'authentication_form': AuthenticationForm},
       name='auth_login'),
   url(r'^logout/$',
       auth_views.logout,
       {'template_name': 'registration/logout.html'},
       name='auth_logout'),
]

if getattr(settings, 'INCLUDE_REGISTER_URL', True):
    urlpatterns += [
        url(r'^customer/register/$',
            CustomerRegistrationView.as_view(),
            name='customer_registration'),
        url(r'^employee/register/$',
            EmployeeRegistrationView.as_view(),
            name='employee_registration'),
    ]

if getattr(settings, 'INCLUDE_AUTH_URLS', True):
    urlpatterns += [
        url(r'', include('registration.auth_urls')),
    ]

# For custom profile page
urlpatterns += [
    url(r'profile/$', customer_profile, name='profile'),
    url(r'profile/employee/$', employee_page, name='employee_profile'),
]