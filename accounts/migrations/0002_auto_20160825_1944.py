# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('accounts', '0001_initial'),
        ('registration', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailRegistrationProfile',
            fields=[
                ('registrationprofile_ptr', models.OneToOneField(serialize=False, primary_key=True, to='registration.RegistrationProfile', parent_link=True, auto_created=True)),
            ],
            options={
                'verbose_name_plural': 'registrations',
                'verbose_name': 'registration',
            },
            bases=('registration.registrationprofile',),
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', to='auth.Group', related_name='user_set', related_query_name='user', blank=True, verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(help_text='Specific permissions for this user.', to='auth.Permission', related_name='user_set', related_query_name='user', blank=True, verbose_name='user permissions'),
        ),
    ]
