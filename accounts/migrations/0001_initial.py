# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(null=True, blank=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status', default=False)),
                ('email', models.EmailField(db_index=True, unique=True, verbose_name='email address (used for login)', max_length=255)),
                ('is_staff', models.BooleanField(help_text='Designates whether the user can log into this admin site.', verbose_name='staff status', default=False)),
                ('is_active', models.BooleanField(help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active', default=True)),
                ('date_joined', models.DateTimeField(verbose_name='date joined', default=django.utils.timezone.now)),
                ('gender', models.CharField(choices=[('f', 'Madam'), ('m', 'Sir'), ('-', 'Madam/Sir')], verbose_name='Gender', max_length=2)),
                ('title', models.IntegerField(choices=[(1, 'Dr.'), (2, 'Prof.')], null=True, blank=True, verbose_name='title')),
                ('first_name', models.CharField(verbose_name='First name', max_length=200)),
                ('last_name', models.CharField(null=True, blank=True, verbose_name='Last name', max_length=200)),
                ('phone', models.CharField(verbose_name='Phone', max_length=200)),
                ('sender_id', django_extensions.db.fields.ShortUUIDField(help_text='Message ID for internal messaging', editable=False, unique=True, verbose_name='Message ID', blank=True)),
                ('language', models.CharField(help_text='Spoken language', choices=[('de', 'German'), ('fr', 'French'), ('it', 'Italian'), ('en', 'English')], default='de', verbose_name='Language', max_length=2)),
                ('preferred_contact_type', models.CharField(choices=[('email', 'email'), ('phone', 'phone'), ('sms', 'sms')], null=True, blank=True, verbose_name='Preferred contact', max_length=20)),
                ('address_street', models.CharField(null=True, blank=True, verbose_name='Street', max_length=100)),
                ('address_house_number', models.CharField(null=True, blank=True, verbose_name='House number', max_length=20)),
                ('address_extra', models.CharField(null=True, blank=True, verbose_name='Address more info', max_length=20)),
                ('address_city', models.CharField(null=True, blank=True, verbose_name='City', max_length=100)),
                ('address_postal_code', models.CharField(null=True, blank=True, verbose_name='ZIP Code', max_length=50)),
                ('address_country', models.CharField(null=True, blank=True, verbose_name='Country', max_length=100)),
            ],
            options={
                'verbose_name_plural': 'users',
                'verbose_name': 'user',
                'swappable': 'AUTH_USER_MODEL',
            },
        ),
    ]
