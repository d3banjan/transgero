# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import hashlib
import random
import re

from django.conf import settings
from django.db import models
from django_extensions.db.fields import ShortUUIDField
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    Group,
    PermissionsMixin,
)
from django.core.mail import send_mail
from django.utils import timezone, six
from django.utils.translation import ugettext_lazy as _

from registration.models import RegistrationProfile, RegistrationManager
from registration.users import UserModel


try:
    from django.utils.timezone import now as datetime_now
except ImportError:
    datetime_now = datetime.datetime.now


SHA1_RE = re.compile('^[a-f0-9]{40}$')

# User related permissions
AGENTS_RW_GROUP_ID = 1


class UserManager(BaseUserManager):

    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """ Create and save an User with the given email and password.

        :param str email: user email
        :param str password: user password
        :param bool is_staff: whether user staff or not
        :param bool is_superuser: whether user admin or not
        :return custom_user.models.User user: user
        :raise ValueError: email is not set

        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        is_active = extra_fields.pop("is_active", True)
        user = self.model(email=email, is_staff=is_staff, is_active=is_active,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """ Create and save an User with the given email and password.

        :param str email: user email
        :param str password: user password
        :return custom_user.models.User user: regular user

        """
        is_staff = extra_fields.pop("is_staff", False)
        return self._create_user(email, password, is_staff, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """ Create and save an User with the given email and password.

        :param str email: user email
        :param str password: user password
        :return custom_user.models.User user: admin user

        """
        return self._create_user(email, password, True, True,
                                 **extra_fields)

    def get_by_natural_key(self, username):
        return self.get(email__iexact=username)


class User(AbstractBaseUser, PermissionsMixin):

    """ Abstract User with the same behaviour as Django's default User.

    AbstractUser does not have username field. Uses email as the
    USERNAME_FIELD for authentication.

    Inherits from both the AbstractBaseUser and PermissionMixin.

    The following attributes are inherited from the superclasses:
        * password
        * last_login
        * is_superuser

    """

    GERMAN = 'de'
    FRENCH = 'fr'
    ITALIAN = 'it'
    ENGLISH = 'en'

    LANGUAGE_CHOICES = (
        (GERMAN, _('German')),
        (FRENCH, _('French')),
        (ITALIAN, _('Italian')),
        (ENGLISH, _('English')),
    )

    GENDER_CHOICES = (
        ('f', _('Madam')),
        ('m', _('Sir')),
        ('-', _('Madam/Sir')),
    )

    TITLE_CHOICES = (
        (1, _('Dr.')),
        (2, _('Prof.')),
    )

    EMAIL = 'email'
    PHONE = 'phone'
    SMS = 'sms'

    CONTACT_CHOICES = (
        (EMAIL, _('email')),
        (PHONE, _('phone')),
        (SMS, _('sms')),
    )

    email = models.EmailField(
        _('email address (used for login)'),
        max_length=255,
        unique=True,
        db_index=True,
    )

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'
        )
    )

    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as '
            'active. Unselect this instead of deleting accounts.'
        )
    )

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    gender = models.CharField(
        _('Gender'),
        max_length=2,
        choices=GENDER_CHOICES,
    )

    title = models.IntegerField(
        _('title'),
        choices=TITLE_CHOICES,
        null=True,
        blank=True,
    )

    first_name = models.CharField(
        _("First name"),
        max_length=200,
    )
    last_name = models.CharField(
        _("Last name"),
        max_length=200,
        null=True,
        blank=True,
    )

    phone = models.CharField(
        _("Phone"),
        max_length=200,
    )

    sender_id = ShortUUIDField(
        _("Message ID"),
        help_text=_("Message ID for internal messaging"),
        unique=True,
    )
    language = models.CharField(
        _('Language'),
        help_text=_("Spoken language"),
        max_length=2,
        choices=LANGUAGE_CHOICES,
        default=GERMAN,
    )

    preferred_contact_type = models.CharField(
        verbose_name=_('Preferred contact'),
        max_length=20,
        choices=CONTACT_CHOICES,
        null=True,
        blank=True,
    )

    address_street = models.CharField(
        _("Street"), null=True, max_length=100, blank=True)
    address_house_number = models.CharField(
        _("House number"), null=True, blank=True, max_length=20)
    address_extra = models.CharField(
        _("Address more info"), null=True, blank=True, max_length=20)
    address_city = models.CharField(
        _("City"), null=True, blank=True, max_length=100)
    address_postal_code = models.CharField(
        _("ZIP Code"), null=True, blank=True, max_length=50)
    address_country = models.CharField(
        _("Country"), null=True, blank=True, max_length=100)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        swappable = 'AUTH_USER_MODEL'

    def __str__(self):
        if self.first_name and self.last_name:
            return u"{} {}".format(self.first_name, self.last_name)
        return self.email

    def get_full_name(self):
        """ Return the email."""
        if self.first_name and self.last_name:
            return u"{} {}".format(self.first_name, self.last_name)
        return self.email

    def get_short_name(self):
        """ Return the email."""
        if self.first_name and self.last_name:
            return "{} {}".format(self.first_name, self.last_name)
        return self.email

    def get_username(self):
        """ Return the email."""
        if self.first_name and self.last_name:
            return "{} {}".format(self.first_name, self.last_name)
        return self.email

    def has_registered(self):
        if hasattr(self, "registrationprofile"):
            return True
        return False

    @property
    def can_edit_agents(self):
        """Check if the user has permission to edit Agents."""
        if self.is_superuser:
            return True

        try:
            self.groups.get(id=AGENTS_RW_GROUP_ID)
            return True
        except Group.DoesNotExist:
            return False


class EmailRegistrationManager(RegistrationManager):
    """
    Custom manager for the ``RegistrationProfile`` model.

    The methods defined here provide shortcuts for account creation
    and activation (including generation and emailing of activation
    keys), and for cleaning out expired inactive accounts.

    """
    def activate_user(self, activation_key):
        """
        Validate an activation key and activate the corresponding
        ``User`` if valid.

        If the key is valid and has not expired, return the ``User``
        after activating.

        If the key is not valid or has expired, return ``False``.

        If the key is valid but the ``User`` is already active,
        return ``False``.

        To prevent reactivation of an account which has been
        deactivated by site administrators, the activation key is
        reset to the string constant ``RegistrationProfile.ACTIVATED``
        after successful activation.

        """
        # Make sure the key we're trying conforms to the pattern of a
        # SHA1 hash; if it doesn't, no point trying to look it up in
        # the database.
        if SHA1_RE.search(activation_key):
            try:
                profile = self.get(activation_key=activation_key)
            except self.model.DoesNotExist:
                return False
            if not profile.activation_key_expired():
                user = profile.user
                if hasattr(settings, "ACTIVATED_AFTER_ACTIVATION") and bool(settings.ACTIVATED_AFTER_ACTIVATION):
                    user.is_active = settings.ACTIVATED_AFTER_ACTIVATION
                    user.save()
                profile.activation_key = self.model.ACTIVATED
                profile.save()
                return user
        return False

    def create_registration_profile(self, new_user, site, send_email=True,
                                    request=None):
        # Delete old existing registration profiles
        EmailRegistrationProfile.objects.filter(user=new_user).delete()

        registration_profile = self.create_profile(new_user)

        if send_email:
            registration_profile.send_activation_email(site, request)

    def create_inactive_user(self, email, password, site, send_email=True,
                             request=None):
        """
        Create a new, inactive ``User``, generate a
        ``RegistrationProfile`` and email its activation key to the
        ``User``, returning the new ``User``.

        By default, an activation email will be sent to the new
        user. To disable this, pass ``send_email=False``.
        Additionally, if email is sent and ``request`` is supplied,
        it will be passed to the email template.

        """
        new_user = UserModel().objects.create_user(email, password)
        new_user.is_active = False
        new_user.save()

        self.create_registration_profile(new_user, site, send_email, request)

        return new_user

    def create_profile(self, user):
        """
        Create a ``RegistrationProfile`` for a given
        ``User``, and return the ``RegistrationProfile``.

        The activation key for the ``RegistrationProfile`` will be a
        SHA1 hash, generated from a combination of the ``User``'s
        username and a random salt.

        """
        salt = hashlib.sha1(
            six.text_type(random.random()).encode('ascii')).hexdigest()[:5]
        salt = salt.encode('ascii')
        email = user.email
        if isinstance(email, six.text_type):
            email = email.encode('utf-8')
        activation_key = hashlib.sha1(salt + email).hexdigest()
        return self.create(user=user, activation_key=activation_key)


class EmailRegistrationProfile(RegistrationProfile):

    objects = EmailRegistrationManager()

    class Meta:
        verbose_name = _('registration')
        verbose_name_plural = _('registrations')
