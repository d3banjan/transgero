from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from .forms import UserChangeForm, UserCreationForm
from .models import User


class HasRegistered(admin.SimpleListFilter):
    """Human-readable title.

    Displayed in the right admin sidebar just above the filter options.
    """

    title = _('Registered?')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'has_registered'

    def lookups(self, request, model_admin):
        """Return a list of tuples.

        The first element in each tuple is the coded value for the option that
        will appear in the URL query. The second element is the
        human-readable name for the option that will appear in the right
        sidebar.
        """
        return (
            ('yes', _('Yes')),
            ('no', _('No')),
        )

    def queryset(self, request, queryset):
        """Return the filtered queryset.

        It's based on the value provided in the query string and retrievable
        via `self.value()`.
        """
        if self.value() == 'yes':
            return queryset.exclude(registrationprofile=None)
        if self.value() == 'no':
            return queryset.filter(registrationprofile=None)


@admin.register(User)
class UserAdmin(UserAdmin):
    """Configuration for the user admin."""

    readonly_fields = ('sender_id', )

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (
            _('Profile'),
            {
                'fields': (
                    'gender',
                    'title',
                    'first_name',
                    'last_name',
                    'language',
                    'sender_id',
                )
            }
        ),
        (
            _('Contact Details'),
            {
                'fields': (
                    'phone',
                    'address_street',
                    'address_house_number',
                    'address_extra',
                    'address_city',
                    'address_postal_code',
                    'address_country',
                )
            }
        ),
        (
            _('Permissions'),
            {
                'fields': (
                    'is_active',
                    'is_staff',
                    'is_superuser',

                    'groups',
                    'user_permissions'
                )
            }
        ),
        (
            _('Important dates'),
            {'fields': ('last_login', 'date_joined')}
        ),

    )
    add_fieldsets = ((
        None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')
        }
    ),
    )

    def has_registered(self, obj):
        """Check if the user has registered or is created by us."""
        return obj.has_registered()
    has_registered.short_description = _('Registered?')
    has_registered.boolean = True

    def has_password(self, obj):
        """Check if the user has set the password."""
        return obj.has_usable_password()
    has_password.short_description = _('Password set?')
    has_password.boolean = True

    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = (
        'email',
        'gender',
        'title',
        'first_name',
        'last_name',
        'phone',
        'is_active',
        'is_staff',
        'is_superuser',
        'has_password',
        'has_registered',
        'date_joined',
        'last_login',
        'sender_id',
    )
    # list_editable = (
    #     'gender',
    #     'title',
    #     'first_name',
    #     'last_name',
    #     'phone',
    #     'is_active',
    # )

    list_filter = (
        'is_staff',
        'is_superuser',
        'is_active',
        'date_joined',
        'last_login',
        'groups', HasRegistered,
    )
    search_fields = ('email', 'first_name', 'last_name', 'phone', 'sender_id')
    ordering = ('-date_joined',)
    filter_horizontal = ('groups', 'user_permissions',)

    list_per_page = 25
