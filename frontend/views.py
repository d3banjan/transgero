# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.conf import settings
from django.views.decorators.http import require_POST
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from frontend.forms import ContactForm, FindCity
from orders.forms import OrderForm
from transgero.tasks import send_mail_template
from employees.models import Employee
import json


def index(request):
    """Landing page."""
    ctx = {}
    ctx['find_city_form'] = FindCity()
    ctx['order_form'] = OrderForm()
    places = Employee.objects.exclude(user__address_city=None).values(
            'user__address_country', 'user__address_city').distinct().order_by(
            'user__address_country')
    ctx['places'] = places
    return render(request, 'frontend/home.html', ctx)


@require_POST
def contact_form_send(request):
    ctx = {}
    ctx['order_form'] = OrderForm()
    contact_form = ContactForm(request.POST)
    if contact_form.is_valid():
        name = contact_form.cleaned_data['name']
        email = contact_form.cleaned_data['email']
        phone = contact_form.cleaned_data['phone']
        message = contact_form.cleaned_data['message']
        status = send_mail_template(
            sender_name=name,
            sender_email=email,
            email_data={'name': name, 'phone': phone, 'message': message},
            recipient_email=settings.CONTACT_EMAIL,
            template_id=settings.CONTACT_US_TEMPLATE,
            tags=['contact_email'],
        )
        if status:
            ctx['contact_form'] = ContactForm()
            ctx['success'] = _("The message has been successfully sent.")
        else:
            ctx['contact_form'] = contact_form
            ctx['error'] = _("Something is wrong with the mail delivery"
                             " system at the moment.")
    else:
        ctx['error'] = _('Oh.. There is something wrong in your contact '
                         'form. Please try again !')
        ctx['contact_form'] = contact_form
    return render(request, 'frontend/home.html', ctx)


@require_POST
def find_city(request):
    """Checking if an employee exist in the city."""
    response = {}
    response['success'] = False
    form = FindCity(request.POST)
    if form.is_valid():
        city = form.cleaned_data['city']
        print(city)
        if Employee.objects.filter(user__address_city=city).count() > 0:
            response['success'] = True
    data = json.dumps(response)
    return HttpResponse(data, 'application/json')


def privacy(request):
    """Privacy for data page."""
    ctx = {}
    return render(request, 'frontend/privacy.html', ctx)


def imprint(request):
    """Imprint page."""
    ctx = {}
    return render(request, 'frontend/imprint.html', ctx)