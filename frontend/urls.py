# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from .views import index, contact_form_send, find_city, privacy, imprint

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^contact/$', contact_form_send, name='contact'),
    url(r'^find_city/$', find_city, name='find_city'),
    url(r'^privacy/$', privacy, name='privacy'),
    url(r'^imprint/$', imprint, name='imprint'),
]
