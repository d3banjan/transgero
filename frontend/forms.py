# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper


class ContactForm(forms.Form):
    name = forms.CharField(
        label=_('Name'),
        required=True,
    )
    phone = forms.CharField(
        label=_("Your Phone"),
        required=False,
    )
    email = forms.EmailField(
        label=_('Your email'),
        required=True,
    )
    message = forms.CharField(
        label=_('Message'),
        required=True,
        widget=forms.Textarea,
    )


class FindCity(forms.Form):

    city = forms.CharField(label=_('City'),
                           required=False,
                           widget=forms.TextInput(
                                   attrs={'placeholder': 'Berlin'}))
