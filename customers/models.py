# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from transgero.models import CreateUpdateModel


class Promotion(models.Model):
    """It contains the details of a promotion code."""

    code = models.CharField(
            _("Code"),
            help_text=_("Promotion"),
            max_length=200,
            unique=True
    )
    description = models.CharField(
        _("Description"),
        help_text=_("Description of the promotion"),
        max_length=200,
        blank=True,
    )

    def __str__(self):
        return "{}".format(self.code)

    class Meta:
        verbose_name = _('Promotion')
        verbose_name_plural = _('Promotions')


class Customer(CreateUpdateModel):
    """This contains the details of a customer."""

    email = models.EmailField(
        _('Email'),
        max_length=255,
        unique=True,
        db_index=True,
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_("User"),
        related_name="customers",
    )
    promotion_code = models.ForeignKey(
        Promotion,
        verbose_name=_('Promotion'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    ratings = models.FloatField(
        _('Ratings'),
        default=3.0,
    )

    def __str__(self):
        return u"{} -- {}".format(self.user.first_name, self.email)

    class Meta:
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')