# -*- coding: utf-8 -*-
from django.shortcuts import render
from .forms import AccountEditForm
from orders.forms import OrderForm
from orders.models import Order
from orders.views import get_employee
from employees.models import Employee
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.cache import never_cache
from transgero.utils import paginated_queryset


def registered_order_form(user):
    
    class RegisteredOrderForm(OrderForm):

        def __init__(self, *args, **kwargs):
            super(RegisteredOrderForm, self).__init__(*args, **kwargs)
            self.fields['phone'].initial = user.phone
            self.fields['email'].initial = user.email
            self.fields['name'].initial = "{} {}".format(
                user.first_name, user.last_name)
            self.fields['address_street'].initial = user.address_street
            self.fields['address_house_number'].initial = \
                user.address_house_number
            self.fields['address_extra'].initial = user.address_extra
            self.fields['address_city'].initial = user.address_city
            self.fields['address_postal_code'].initial = \
                user.address_postal_code
            self.fields['address_country'].initial = user.address_country

    return RegisteredOrderForm()


@never_cache
def customer_profile(request):
    """Profile page."""
    ctx = {}
    user = request.user
    if request.user.is_authenticated():
        all_orders = Order.objects.filter(customer__email=user.email)
        ctx['current_orders'] = all_orders.filter(
                status__in=Order.DISPLAY_CRITERIA).order_by('-created')
        orders = all_orders.exclude(
                status__in=Order.DISPLAY_CRITERIA).order_by('-created')
        page = request.GET.get('page')
        ctx['orders'], ctx['pages'], ctx['current_page'] = \
            paginated_queryset(orders, page)

        if get_employee(request):
            ctx['is_employee'] = True
        ctx['account_edit_form'] = AccountEditForm(instance=user)
        if request.method == 'POST':
            form = AccountEditForm(request.POST, instance=user)
            if form.is_valid():
                user = form.save()
                ctx['success'] = _('Profile has been updated.')
            else:
                ctx['account_edit_form'] = form
                ctx['error'] = _('Profile could not be updated. '
                                 'Please try again !')
        ctx['order_form'] = registered_order_form(user)
    return render(request, 'accounts/profile.html', ctx)
