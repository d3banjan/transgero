# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import Customer, Promotion


@admin.register(Promotion)
class PromotionAdmin(admin.ModelAdmin):

    list_display = ('code',
                    'description',)
    search_fields = ('code', )
    ordering = ('-id', )
    list_per_page = 25


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):

    def user_link(self, obj):
        """The admin link of the user."""
        return "<a href='/admin/accounts/user/{}'>{} {}</a>".format(
            obj.user.id, obj.user.first_name, obj.user.last_name)
    user_link.short_description = _('User')
    user_link.allow_tags = True

    list_display = ('email',
                    'user_link',
                    'ratings',
                    'promotion_code',)
    list_filter = ('promotion_code', )
    search_fields = ('email', )
    ordering = ('-id', )
    list_per_page = 25
