# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(verbose_name='Modified', auto_now=True)),
                ('email', models.EmailField(db_index=True, max_length=255, unique=True, verbose_name='Email')),
                ('ratings', models.FloatField(default=3.0, verbose_name='Ratings')),
            ],
            options={
                'verbose_name': 'Customer',
                'verbose_name_plural': 'Customers',
            },
        ),
        migrations.CreateModel(
            name='Promotion',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('code', models.CharField(help_text='Promotion', max_length=200, unique=True, verbose_name='Code')),
                ('description', models.CharField(help_text='Description of the promotion', max_length=200, blank=True, verbose_name='Description')),
            ],
            options={
                'verbose_name': 'Promotion',
                'verbose_name_plural': 'Promotions',
            },
        ),
        migrations.AddField(
            model_name='customer',
            name='promotion_code',
            field=models.ForeignKey(null=True, verbose_name='Promotion', blank=True, on_delete=django.db.models.deletion.SET_NULL, to='customers.Promotion'),
        ),
        migrations.AddField(
            model_name='customer',
            name='user',
            field=models.ForeignKey(verbose_name='User', to=settings.AUTH_USER_MODEL, related_name='customers'),
        ),
    ]
