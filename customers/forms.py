# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _


class AccountEditForm(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = ['email', 'gender', 'title', 'first_name', 'last_name',
                  'phone', 'language', 'preferred_contact_type',
                  'address_street', 'address_house_number',
                  'address_extra', 'address_city', 'address_postal_code',
                  'address_country', ]
