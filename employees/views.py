# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.decorators.cache import never_cache
from orders.models import Order
from customers.views import paginated_queryset
from customers.forms import AccountEditForm
from .models import Employee
from orders.views import get_employee


@never_cache
def employee_page(request):
    """Employee page."""
    ctx = {}
    user = request.user
    if request.user.is_authenticated():
        if request.GET.get('create'):
            employee, created = Employee.objects.get_or_create(
                user=user,
                email=user.email,
                department=Employee.DEPT_DELIVERY,
            )
        else:
            orders = Order.objects.filter(
                    employee__email=user.email).order_by('-created')
            page = request.GET.get('page')
            ctx['orders'], ctx['pages'], ctx['current_page'] = \
                paginated_queryset(orders, page)

        if get_employee(request):
            ctx['is_employee'] = True
        ctx['account_edit_form'] = AccountEditForm(instance=user)
        if request.method == 'POST':
            form = AccountEditForm(request.POST, instance=user)
            if form.is_valid():
                user = form.save()
                ctx['success'] = _('Profile has been updated.')
            else:
                ctx['account_edit_form'] = form
                ctx['error'] = _('Profile could not be updated. '
                                 'Please try again !')
    return render(request, 'accounts/employee_profile.html', ctx)