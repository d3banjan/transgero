# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from transgero.models import CreateUpdateModel


class Employee(CreateUpdateModel):
    """This contains the details of an employee."""

    DEPT_OTHER = 0
    DEPT_DEVELOPMENT = 1
    DEPT_SALES = 2
    DEPT_CUSTOMER_SUPPORT = 3
    DEPT_MARKETING = 4
    DEPT_PRODUCT = 5
    DEPT_MANAGEMENT = 6
    DEPT_DELIVERY = 7
    DEPT_CUSTOMER = 8

    DEPARTMENT_CHOICES = (
        (DEPT_DELIVERY, _('Delivery Support')),
        (DEPT_DEVELOPMENT, _('Development')),
        (DEPT_SALES, _('Sales')),
        (DEPT_CUSTOMER_SUPPORT, _('Customer Support')),
        (DEPT_MARKETING, _('Marketing')),
        (DEPT_PRODUCT, _('Product')),
        (DEPT_MANAGEMENT, _('Management')),
        (DEPT_CUSTOMER, _('Customer')),
        (DEPT_OTHER, _('Other')),
    )

    email = models.EmailField(
        _('Email'),
        max_length=255,
        unique=True,
        db_index=True,
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_("User"),
        related_name="employees",
    )
    department = models.IntegerField(
        _('Department'),
        choices=DEPARTMENT_CHOICES,
        null=True,
        blank=True,
    )
    active_status = models.BooleanField(
        _('Active'),
        default=True,
        help_text=_(
            'Designates if the employee is still active for service.'
        )
    )
    approved = models.BooleanField(
        _('Approved'),
        default=False,
        help_text=_(
            'The profile has been verified.'
        )
    )
    ratings = models.FloatField(
        _('Ratings'),
        default=3.0,
    )

    def __str__(self):
        return u"{} -- {}".format(self.user.first_name, self.email)

    class Meta:
        verbose_name = _('Employee')
        verbose_name_plural = _('Employees')