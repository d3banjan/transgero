# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(verbose_name='Modified', auto_now=True)),
                ('email', models.EmailField(db_index=True, max_length=255, unique=True, verbose_name='Email')),
                ('department', models.IntegerField(null=True, blank=True, verbose_name='Department', choices=[(7, 'Delivery Support'), (1, 'Development'), (2, 'Sales'), (3, 'Customer Support'), (4, 'Marketing'), (5, 'Product'), (6, 'Management'), (8, 'Customer'), (0, 'Other')])),
                ('active_status', models.BooleanField(help_text='Designates if the employee is still active for service.', default=True, verbose_name='Active')),
                ('approved', models.BooleanField(help_text='The profile has been verified.', default=False, verbose_name='Approved')),
                ('ratings', models.FloatField(default=3.0, verbose_name='Ratings')),
                ('user', models.ForeignKey(verbose_name='User', to=settings.AUTH_USER_MODEL, related_name='employees')),
            ],
            options={
                'verbose_name': 'Employee',
                'verbose_name_plural': 'Employees',
            },
        ),
    ]
