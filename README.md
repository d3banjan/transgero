# No License #
This repository is merely a showcase for some code from the developer. No one is allowed to copy or derive this work any further at this time. 

We explicitly forbid anyone to copy entire classes and models from this code. 

[What does unlicensed software mean?](https://choosealicense.com/no-license/)

# Transgero #
The website is a marketplace for people who want to sell or consume services. 
And we mean "services" in the widest sense of the word - from buying and selling products online to asking someone to bring you ice-cream from the local ice-cream store.

This website is written in Django. The templates are built using Sass and Pug. The build system is Grunt.js. 