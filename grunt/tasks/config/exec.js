module.exports = function (grunt) {

  var protractorConfigFile = 'protractor.config.js';
  var options = protractorConfigFile + ' --verbose';

  grunt.config.set('exec', {
    'protractor-all': {
      command: 'protractor ' + options
    },
    'protractor-landing-page': {
      command: 'protractor --specs **/protractor/landing_page/*.spec.js ' + options
    },
    'protractor-agent-profile': {
      command: 'protractor --specs **/protractor/agent_profile/*.spec.js ' + options
    },
    'protractor-agent-search': {
      command: 'protractor --specs **/protractor/search/*.spec.js ' + options
    },
    'protractor-states': {
      command: 'protractor --specs **/protractor/states/*.spec.js ' + options
    }
  });

  grunt.loadNpmTasks( "grunt-exec" );
};
