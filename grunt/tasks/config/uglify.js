module.exports = function (grunt) {

  grunt.config.set('uglify', {
    options: {
      beautify: false,
      mangle: true
    },
    main: {
      src: 'assets/js/src/main.js',
      dest: 'static/js/src/main.js'
    }
  });

  grunt.loadNpmTasks( "grunt-contrib-uglify" );
};
