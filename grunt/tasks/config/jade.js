module.exports = function (grunt) {

  grunt.config.set('jade', {
    django: {
      options: {
        pretty: true
      },
      files: [{
        expand: true,
        src: ['**/*.jade', '!**/*.mixins.jade'],
        dest: 'templates/',
        cwd: 'templates',
        ext: '.html'
      }]
    },
    angular: {
      files: [{
        expand: true,
        src: ['**/*.jade', '!**/*.mixins.jade'],
        dest: 'static/templates/angular/',
        cwd: 'assets/js/src/apps',
        ext: '.html'
      }]
    }
  });

  grunt.loadNpmTasks( 'grunt-contrib-jade' );
};
