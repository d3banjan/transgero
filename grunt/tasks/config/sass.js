module.exports = function (grunt) {

  grunt.config.set('sass', {
    dist: {
      files: {
        'static/css/styles.css': 'static/sass/styles.scss'
      }
    }
  });

  grunt.loadNpmTasks( "grunt-contrib-sass" );
};
