module.exports = function (grunt) {

  var testArgs = [ '--noinput' ];

  grunt.config.set('django-manage', {
    test: {
      options: {
        command: 'test',
        args: testArgs,
        verbose: true
      }
    },
    create_test_database: {
      options: {
        command: 'create_test_database',
        verbose: true
      }
    },
    'test-agent-search': {
      options: {
        command: 'test',
        args: testArgs.concat(['search']),
        verbose: true
      }
    },
    'test-immo': {
      options: {
        command: 'test',
        args: testArgs.concat(['immo']),
        verbose: true
      }
    },
    'test-inquiry': {
      options: {
        command: 'test',
        args: testArgs.concat(['inquiry']),
        verbose: true
      }
    },
    'test-api': {
      options: {
        command: 'test',
        args: testArgs.concat(['api']),
        verbose: true
      }
    },
    'test-address': {
      options: {
        command: 'test',
        args: testArgs.concat(['address']),
        verbose: true
      }
    }
  });

  grunt.loadNpmTasks( 'grunt-contrib-django' );
};
