module.exports = function (grunt) {

  grunt.config.set('htmlmin', {
    prod: {
      options: {
        removeComments: true,
        collapseWhitespace: true
      },
      files: {
        'templates/search/search_results_ajax.html': 'templates/search/search_results_ajax.html'
      }
    }
  });

  grunt.loadNpmTasks( 'grunt-contrib-htmlmin' );
};
