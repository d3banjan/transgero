module.exports = function (grunt) {

  var watchFilesBackend = [
    "Gruntfile.js",
    "**/*.py",
    "**/*.json",
    "**/*.jade",
    "**/*.scss",
    "config/tasks/*.js",
    "!node_modules/**",
    "!bower_components/**",
    "*.git/"
  ];

  var watchFilesFrontend = [
    "**/*.jade",
    "**/*.scss",
    "**/*.js",
    "**/*.json",
    "**/*.py",
    "!node_modules/**",
    "!bower_components/**",
    "!static/vendor/**",
    "!static/js/**"
  ];

  var watchFilesAssets= [
    "**/*.jade",
    "**/*.scss",
    "**/*.js",
    "!node_modules/**",
    "!bower_components/**",
    "!static/js/**",
    "!static/vendor/**"
  ];

  var watchOptions = {
    interrupt: true,
    debounceDelay: 250
  };

  grunt.config.set('watch', {
    all: {
      options: watchOptions,
      files: watchFilesBackend,
      tasks: [ "all" ]
    },
    assets: {
      options: watchOptions,
      files: watchFilesAssets,
      tasks: [ "assets" ]
    },
    "test-backend": {
      options: watchOptions,
      files: watchFilesBackend,
      tasks: [ "test-backend" ]
    },
    "test-backend-unit": {
      options: watchOptions,
      files: watchFilesBackend,
      tasks: [ "test-backend-unit" ]
    },
    "test-frontend": {
      options: watchOptions,
      files: watchFilesFrontend,
      tasks: [ "assets", "test-frontend" ]
    },
    "test-frontend-unit": {
      options: watchOptions,
      files: watchFilesFrontend,
      tasks: [ "assets", "test-frontend-unit" ]
    },
    "test-frontend-reload-db": {
      options: watchOptions,
      files: watchFilesFrontend,
      tasks: [ "assets", "test-frontend-reload-db" ]
    },
    "test-frontend-landing-page": {
      options: watchOptions,
      files: watchFilesFrontend,
      tasks: [ "assets", "exec:protractor-landing-page" ]
    },
    "test-frontend-agent-search": {
      options: watchOptions,
      files: watchFilesFrontend,
      tasks: [ "assets", "exec:protractor-agent-search" ]
    },
    "test-frontend-agent-profile": {
      options: watchOptions,
      files: watchFilesFrontend,
      tasks: [ "assets", "exec:protractor-agent-profile" ]
    },
    "test-frontend-states": {
      options: watchOptions,
      files: watchFilesFrontend,
      tasks: [ "assets", "exec:protractor-states" ]
    },
    "test-backend-agent-search": {
      options: watchOptions,
      files: watchFilesBackend,
      tasks: [ "django-manage:test-agent-search" ]
    },
    "test-backend-immo": {
      options: watchOptions,
      files: watchFilesBackend,
      tasks: [ "django-manage:test-immo" ]
    },
    "test-backend-inquiry": {
      options: watchOptions,
      files: watchFilesBackend,
      tasks: [ "django-manage:test-inquiry" ]
    },
    "test-backend-address": {
      options: watchOptions,
      files: watchFilesBackend,
      tasks: [ "django-manage:test-address" ]
    },
    "test-backend-api": {
      options: watchOptions,
      files: watchFilesBackend,
      tasks: [ "django-manage:test-api" ]
    }
  });

  grunt.loadNpmTasks( "grunt-contrib-watch" );
};
