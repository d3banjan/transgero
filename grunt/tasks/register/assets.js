module.exports = function (grunt) {

  grunt.registerTask('assets', [
    'jade',
    'htmlmin',
    'sass',
    'uglify',
    'cssmin'
  ]);

};
