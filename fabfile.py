# -*- coding: utf-8 -*-
import os
from time import sleep
from datetime import datetime
import json
import random
from contextlib import contextmanager
import requests
from fabric.colors import green, yellow, red, cyan, white, blue
from fabric.contrib.console import confirm
from fabric.api import runs_once, lcd, local, task
from fabric.operations import prompt


APP_NAME = 'transgero'
USERNAME = 'transgero'
PASSWORD = '**transgero**'

DJANGO_SETTINGS_MODULE = '%s.settings' % APP_NAME

SLACK_URL = None

installations = {
    'master': 'master',            # PRODUCTION
    'staging': 'staging',          # STAGING (things REALLY should work here)
    'development': 'development',  # DEV (things get merged here)
}

heroku_app_base_url = 'https://%s-%s.herokuapp.com'

urls = {
    'master': heroku_app_base_url % (APP_NAME, 'master'),
    'staging': heroku_app_base_url % (APP_NAME, 'staging'),
    'development': heroku_app_base_url % (APP_NAME, 'development'),
}


'''########################################################################
#######                     General FABRIC tasks                      #####
########################################################################'''


@task
def heroku_login():
    """ For Heroku logging in."""
    local('heroku login')


@task
def create_env(env_name):
    """Create a new instance."""
    heroku_login()
    bar_lenght = 20 + len(env_name)
    print(green(" "))
    print(green("=" * bar_lenght))
    print(white('Creating new environment: {}'.format(env_name)))
    print(green("=" * bar_lenght))
    data = dict(
            app_name=APP_NAME,
            env_name=env_name,
            branch_name=env_name
        )
    cmd = 'heroku apps:create {app_name}-{env_name} --remote {branch_name}' \
          ' --region eu --buildpack heroku/python'.format(**data)

    local(cmd)
    git_url = 'https://git.heroku.com/%s-%s.git' % (APP_NAME, env_name)
    print(git_url)
    # add remote


@task
def copy_env(source, deployment):
    """Copy environment to a new one."""
    pass


@task
def createsuperuser(deployment):
    """Create a superuser for a Django app deployed in Heroku."""
    heroku_login()
    local(
        ('heroku run "python manage.py createsuperuser" '
         '--app {}-{}').format(
            APP_NAME, deployment)
    )


@task
def push_and_deploy(deployment, force_push=False):
    """Push code to Git and then push to Heroku for deployment."""
    bar_lenght = 20 + len(deployment)
    print(green(" "))
    print(green("=" * bar_lenght))
    print(white('Pushing to Heroku: {}'.format(deployment)))
    print(green("=" * bar_lenght))

    app_name_branch = '%s-%s' % (APP_NAME, deployment)
    heroku_login()

    all_git_branch = local("git branch", capture=True).split('\n')
    current_branch = None
    for br in all_git_branch:
        if '*' in br:
            current_branch = br[2:]
            print(yellow("Current branch: {}".format(current_branch)))
    if not current_branch:
        print(red("No git branch initiated"))
        return
    if current_branch != deployment:
        print(red("Branch mismatch: deploying wrong branch to {}".format(
            app_name_branch)))
        return

    local_push_cmd = 'git push origin {}'.format(deployment)
    add_remote = 'heroku git:remote -a {}'.format(app_name_branch)
    push_cmd = 'git push heroku {}:master'.format(deployment)

    if force_push:
        local_push_cmd = "%s -f" % local_push_cmd
        push_cmd = "%s -f" % push_cmd

    print(green('Pushing to Git .....'))
    local(local_push_cmd)

    print(green('Adding remote Heroku app .....'))
    local(add_remote)

    print(green('Pushing to Heroku which will trigger a deployment...'))
    local(push_cmd)

    password = ""
    if not deployment == "master":
        # Username and password are same as App name
        password = "\n(Username/Password: {} / {})".format(USERNAME, PASSWORD)

    message = (
        "*** New Deployment of *<%s|%s>*  %s" % (
            urls[deployment],
            deployment,
            password,
        ))
    print(green(message))
    # send_to_slack(message)


def _get_db_url(environment):
    """Get the database URL."""
    return get_config_value(environment, 'DATABASE_URL')


@task
def getconfig(environment):
    """Get the configuration of an environment."""
    heroku_login()
    app_name_branch = '%s-%s' % (APP_NAME, environment)
    local('heroku config --app %s' % app_name_branch)


@task
def get_config_value(environment, variable):
    """Get the value of a environment value."""
    heroku_login()
    app_name_branch = '%s-%s' % (APP_NAME, environment)
    db_url = local(
        'heroku config:get %s --app %s' % (variable, app_name_branch),
        capture=True,
    )
    if db_url:
        return db_url
    else:
        print(red("Invalid environment, variable, or both."))


# # TODO: is openredis still required?
# @task
# def set_heroku_config():
#     """Set the configuration for an environment in Heroku."""
#     local(
#         ('heroku config:set ON_HEROKU=True OPENREDIS_URL={} SENTRY_DSN={} '
#          'DJANGO_SETTINGS_MODULE={}').format(
#             os.getenv('OPENREDIS_URL', ''),
#             os.getenv('SENTRY_DSN', ''),
#             DJANGO_SETTINGS_MODULE)
#     )


# TODO: dump it compressed
@task
def dump_db(deployment, filename):
    """Dump the database of a remote environment."""
    local('pg_dump --clean {} > {}'.format(_get_db_url(deployment),
                                           os.path.expanduser(filename)))


# TODO: load it compressed
@task
def load_db(environment, filename):
    """Load a database into a remote environment."""
    if confirm(
            green('Load %s to %s?' % (filename, environment)), default=False):
        local('psql "{}" < {}'.format(
            _get_db_url(environment),
            os.path.expanduser(filename)
        ))


@task
def pg(deployment):
    """Connect to the Postgres console in an environment."""
    heroku_login()
    local('psql {}'.format(_get_db_url(deployment)))


def generate_secret_key():
    """Generate a secret key for an app."""
    return ''.join(
        [random.SystemRandom().choice(
            'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)')
         for i in range(50)]
    )


@task
def print_secret_key():
    """Print the secret key of an app."""
    secret_key = generate_secret_key()
    print(yellow(secret_key))


@task
def reset_local_database(database):
    """Reset the local database."""
    print(green(
        "Create Backup for local database {} to local_backup.sql ...".format(
            database)))
    local('sudo pg_dump --clean {} > local_backup.sql'.format(database))
    print(green("Dropping database {} ...".format(database)))
    local("sudo psql -c 'drop database {}'".format(database))
    print(green("Creating database {} ...".format(database)))
    local("sudo psql -c 'create database {}'".format(database))
    # print(green("Loading test data from test_data.sql ..."))
    # local('sudo psql -d {} -f test_data.sql'.format(database))
    print(green("Doing migrations ..."))
    local('sudo python manage.py migrate')



@task
def send_to_slack(message):
    """Send a message to Slack."""
    if SLACK_URL:
        payload = {
            "text": message
        }
        requests.post(url=SLACK_URL, data=json.dumps(payload))

# =============================================================================
# Heroku specific
# =============================================================================


@task
def move_to_heroku(deployment, heroku_deployment):
    """Create a new Heroku application."""
    # Creating addon
    local('heroku create %s --region eu' % heroku_deployment)

    # Installing addons
    local(
        'heroku addons:create heroku-postgresql:hobby-basic --app %s'
        % heroku_deployment)
    local(
        'heroku addons:create heroku-redis:hobby-dev --app %s'
        % heroku_deployment)
    local('heroku addons:create mailgun:starter --app %s' % heroku_deployment)
    local('heroku addons:create newrelic:wayne --app %s' % heroku_deployment)
    # local(
    #    'heroku addons:create librato:development --app %s'
    #    % heroku_deployment)
    local(
        'heroku addons:create scheduler:standard --app %s' % heroku_deployment)

    # Add collaborators
    # TODO: prompt for collaborators
    local(
        'heroku sharing:add kontakt@jensneuhaus.de --app %s'
        % heroku_deployment)

    # Add domain
    # local(
    #    'heroku domains:add %s.domain.de --app %s'
    #    % (deployment, heroku_deployment))

    env_vars = [
        'DATABASE_URL',
        'OPENREDIS_URL',
        'CCTRL_USER',
        'CCTRL_PASSWORD',
        'RQ_OPENREDIS_URL',
        'RQ_REDIS_URL'
    ]

    # Transfering config
    for key, value in sorted(getconfig(deployment).iteritems()):

        if key in env_vars:
            continue

        print("%s=%s" % (key, value))

    if not confirm('Proceed?', default=False):
        return

    for key, value in sorted(getconfig(deployment).iteritems()):

        if key in env_vars:
            continue

        if key == 'APP_NAME':
            local(
                'heroku config:set NEW_RELIC_APP_NAME="%s" --app %s'
                % (value, heroku_deployment)
            )

        local(
            'heroku config:set %s="%s" --app %s'
            % (key, value, heroku_deployment))

    # Setting some heroku-specific config
    local(
        'heroku config:set PAAS_VENDOR="heroku" --app %s' % heroku_deployment
    )
    local('heroku config:set SIZE=3 --app %s' % heroku_deployment)
    local(
        'heroku config:set DEP_NAME=metronom/%s --app %s'
        % (deployment, heroku_deployment)
    )

    # Setting up DB backups at 2am
    local(
        'heroku pg:backups schedule DATABASE_URL --at "02:00 UTC" --app %s'
        % heroku_deployment
    )

    # Enabling the labs
    local(
        'heroku labs:enable log-runtime-metrics --app %s' % heroku_deployment)

    # Creating GIT ref
    local(
        'git remote add %s https://git.heroku.com/%s.git'
        % (deployment, heroku_deployment)
    )

    # Pushing...
    print(green('pushing {}'.format(deployment)))
    local('git push %s heroku:master' % deployment)

    # # Set maintenance mode on the cloudcontrol instance, getting moved
    # Here you have to install a maintenance Tool or something!!

    # print(green('Setting maintenance mode for {}...'.format(deployment)))
    #
    # setconfig(
    #     deployment,
    #     MAINTENANCE=1
    # )

    # Dump the cloudcontrol database
    fn = "/tmp/move_{}_to_heroku.out".format(deployment)
    dump_db(deployment, fn)

    # Load the heroku database
    load_db(heroku_deployment, fn)

    # Deploying the new heroku instance with the hobby tier
    local('heroku ps:type hobby --app %s' % heroku_deployment)

    local('heroku open --app %s' % heroku_deployment)

    print("Remember:")
    print("- set the scheduler, if any cronjobs")
    print("- change the DNS on dnssimple.")
    print("- Setup RQ")
    print("- Move Cloudinary")
