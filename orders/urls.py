# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from .views import (
    order_save,
    customer_order_details,
    employee_order_details,
    all_orders,
    employee_order_process,
    customer_order_process,
    customer_offer_accept,
    unregistered_order_process,
)

urlpatterns = [
    url(r'^order_save/$', order_save, name='order_save'),
    url(r'^customer/order_details/(?P<order_id>.+)/$', customer_order_details,
        name='customer_order_details'),
    url(r'^employee/order_details/(?P<order_id>.+)/$', employee_order_details,
        name='employee_order_details'),
    url(r'^all/$', all_orders, name='all_orders'),
    url(r'^customer/order_process/(?P<order_id>.+)/$', customer_order_process,
        name='customer_order_process'),
    url(r'^anonymous/order_process/(?P<order_encrypted_id>.+)/$',
        unregistered_order_process,
        name='customer_order_process'),
    url(r'^employee/order_process/(?P<order_id>.+)/$', employee_order_process,
        name='employee_order_process'),
    url(r'^customer/offer_accept/(?P<offer_id>.+)/$', customer_offer_accept,
        name='customer_offer_accept'),
]
