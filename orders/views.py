# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.conf import settings
from django.views.decorators.http import require_POST
from .forms import (
    OrderForm,
    RegisteredCustomerOrderForm,
    RegisteredSenderOrderForm,
    EmployeeOrderProcessForm,
)
from .models import Order, OrderOffer
from customers.models import Customer
from employees.models import Employee
from frontend.forms import ContactForm
from customers.forms import AccountEditForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from transgero.utils import (
    paginated_queryset,
    decrypt,
    long_encryption,
    long_decryption
)
from transgero.tasks import send_mail_template
import stripe
import calendar


ORDERS_PER_PAGE = 20


def create_address(order):
    """Creating a complete address from model instance."""
    address = "{} {}<br>{} {}<br>{}".format(
        order.address_street,
        order.address_house_number,
        order.address_city,
        order.address_postal_code,
        order.address_country
    )
    return address


def get_employee(request):
    """Returning an authenticated employee."""
    if request.user.is_authenticated():
        user = request.user
        try:
            return Employee.objects.select_related().get(
                email=user.email, user=user,
                approved=True, active_status=True)
        except Employee.DoesNotExist:
            return None


def get_customer(request):
    """Returning an authenticated customer."""
    if request.user.is_authenticated():
        user = request.user
        try:
            return Customer.objects.select_related().get(
                email=user.email, user=user)
        except Customer.DoesNotExist:
            return None


@require_POST
def order_save(request):
    """Saves the order form."""
    ctx = {}
    ctx['contact_form'] = ContactForm()
    order_form = OrderForm(request.POST)
    customer = get_customer(request)
    order = None

    if order_form.is_valid():
        order = order_form.save(commit=False)
        order.customer = get_customer(request)
        order.save()
        ctx['order_form'] = OrderForm()
        ctx['success'] = _('Great ! Your order is in process now ...')
    else:
        ctx['error'] = _('Oh.. There is something wrong in your order. '
                         'Please try again !')
        ctx['order_form'] = order_form

    if customer:
        # Checking if the user is logged in.
        ctx['account_edit_form'] = AccountEditForm(instance=request.user)
        customer_orders = Order.objects.filter(customer__email=customer.email)
        ctx['current_orders'] = customer_orders.filter(
                status__in=Order.DISPLAY_CRITERIA).order_by('-created')
        orders = customer_orders.exclude(
                status__in=Order.DISPLAY_CRITERIA).order_by('-created')
        page = request.GET.get('page')
        ctx['orders'], ctx['pages'], ctx['current_page'] = \
            paginated_queryset(orders, page)
        return render(request, 'accounts/profile.html', ctx)
    elif order:
        email_data = {
            "order_time": "{} {} {} - {}:{}".format(
                order.created.day,
                calendar.month_name[order.created.month],
                order.created.year,
                order.created.hour,
                order.created.minute
            ),
            "site": "TRANSGERO.com",
            "order_encrypted_id": long_encryption(order.id),
            "user": order.name,
            "address": create_address(order),
        }
        send_mail_template(
            sender_name="Trangero.com",
            sender_email=settings.CONTACT_EMAIL,
            recipient_email=order.email,
            template_id=settings.ORDER_TEMPLATE,
            tags=["Order", ],
            email_data=email_data,
        )
    return render(request, 'frontend/home.html', ctx)


@login_required
def customer_order_details(request, order_id):
    """Displays the details of an order of a customer."""
    ctx = {}
    try:
        order = Order.objects.select_related().get(display_hashid=order_id)

        if get_customer(request) == order.customer:
            ctx['order'] = order
            ctx['order_customer_form'] = RegisteredCustomerOrderForm()
            if request.method == 'POST':
                customer_form = RegisteredCustomerOrderForm(
                    request.POST, instance=order)
                if customer_form.is_valid():
                    customer_form.save()
                    ctx['success'] = _('The order has been updated.')
                else:
                    ctx['order_customer_form'] = customer_form
                    ctx['error'] = _("Order could not be updated !")

    except Order.DoesNotExist:
        ctx['error'] = _("Order could not be found !")
    return render(request, 'orders/order_details.html', ctx)


@login_required
def employee_order_details(request, order_id):
    """Displays the details of an order of an employee."""
    ctx = {}
    try:
        order = Order.objects.select_related().get(display_hashid=order_id)

        if get_employee(request) == order.employee:
            ctx['order'] = order
            if order.status in Order.DISPLAY_CRITERIA:
                # to prevent editing any information for an order
                #  which has been already completed
                ctx['order_employee_form'] = RegisteredSenderOrderForm()
                if request.method == 'POST':
                    order_employee_form = RegisteredSenderOrderForm(
                            request.POST)
                    if order_employee_form.is_valid():
                        delivery_status = order_employee_form.cleaned_data[
                            'delivery_status']
                        if delivery_status:
                            order.status = Order.DELIVERED
                            order.save()
                        ctx['success'] = _('The order has been updated.')
                        ctx['order_employee_form'] = None
                    else:
                        ctx['order_employee_form'] = order_employee_form
                        ctx['error'] = _("Order could not be updated !")

    except Order.DoesNotExist:
        ctx['error'] = _("Order could not be found !")
    return render(request, 'orders/order_details.html', ctx)


def all_orders(request):
    """Displays all orders or specific to the employee's city."""
    ctx = {}
    employee = get_employee(request)
    if employee:
        city = employee.user.address_city
        orders = Order.objects.filter(
            status__in=Order.DISPLAY_CRITERIA,
            address_city=city
        ).order_by('-created')
        ctx['city'] = city
    else:
        orders = Order.objects.filter(
                status__in=Order.DISPLAY_CRITERIA).order_by('-created')
    page = request.GET.get('page')
    ctx['orders'], ctx['pages'], ctx['current_page'] = \
        paginated_queryset(orders, page)
    return render(request, 'orders/orders.html', ctx)


@login_required
def employee_order_process(request, order_id):
    """Displays a page where bidding takes place for an order."""
    ctx = {}
    try:
        order = Order.objects.select_related().get(display_hashid=order_id)
        if order.status == Order.OPEN:
            ctx['order'] = order

            employee = get_employee(request)
            if employee:
                ctx['is_employee'] = True
                ctx['order_process_form'] = EmployeeOrderProcessForm()
                if request.method == 'POST':
                    order_process_form = EmployeeOrderProcessForm(request.POST)
                    if order_process_form.is_valid():
                        offer = order_process_form.save(commit=False)
                        offer.employee = employee
                        offer.order = order
                        offer.save()
                        ctx['success'] = _('An offer has been made.')
                    else:
                        ctx['order_process_form'] = order_process_form
                        ctx['error'] = _("Please check your offer !")
            ctx['offers'] = OrderOffer.objects.select_related(
                ).filter(order__id=order.id)

    except Order.DoesNotExist:
        ctx['error'] = _("Order could not be found !")
    return render(request, 'orders/order_process.html', ctx)


@login_required
def customer_order_process(request, order_id):
    """Displays page where customer accepts/declines an offer for an order."""
    ctx = {}
    try:
        order = Order.objects.select_related().get(display_hashid=order_id)
        if order.status == Order.OPEN:
            ctx['order'] = order
            if get_customer(request) == order.customer:
                ctx['is_customer'] = True
                ctx['offers'] = OrderOffer.objects.select_related(
                        ).filter(order__id=order.id)

    except Order.DoesNotExist:
        ctx['error'] = _("Order could not be found !")
    return render(request, 'orders/order_process.html', ctx)


# @login_required
def customer_offer_accept(request, offer_id):
    """API for accepting an offer."""
    ctx = {}
    try:
        offer = OrderOffer.objects.select_related().get(id=decrypt(offer_id))
        order = offer.order
        if (order.status == Order.OPEN
            and order.final_fee == 0.0
            and not order.employee):
            offer.status = True
            offer.save()
            order.employee = offer.employee
            order.status = Order.DELIVERY_IN_PROGRESS
            order.final_fee = offer.fee
            order.transgero_charges = settings.TRANSGERO_CHARGE * offer.fee
            order.save()
            ctx['order'] = order
            if get_customer(request) == order.customer:
                ctx['is_customer'] = True
                ctx['offers'] = OrderOffer.objects.select_related(
                        ).filter(order__id=order.id)

    except Order.DoesNotExist:
        ctx['error'] = _("Order could not be found !")
    return render(request, 'orders/order_process.html', ctx)


def unregistered_order_process(request, order_encrypted_id):
    """Tracking order from unregistered customer."""
    ctx = {}
    try:
        order = Order.objects.select_related().get(
            id=long_decryption(order_encrypted_id))
        if order.status == Order.OPEN:
            ctx['order'] = order
            ctx['is_customer'] = True
            ctx['offers'] = OrderOffer.objects.select_related(
                ).filter(order__id=order.id)
    except Order.DoesNotExist:
        ctx['error'] = _("Order could not be found or have been processed !")
    return render(request, 'orders/order_process.html', ctx)

