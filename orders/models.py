# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from transgero.utils import encrypt
from transgero.models import CreateUpdateModel
from customers.models import Customer, Promotion
from employees.models import Employee


class Order(CreateUpdateModel):
    """This contains the details of an order."""

    OPEN = 0
    DELIVERY_IN_PROGRESS = 1
    DELIVERED = 2
    CLOSED = 3
    STATUS_CHOICES = (
        (OPEN, _('Open')),
        (DELIVERY_IN_PROGRESS, _('Delivery in progress')),
        (DELIVERED, _('Delivered')),
        (CLOSED, _('Closed')),
    )
    DISPLAY_CRITERIA = [OPEN, DELIVERY_IN_PROGRESS]

    EMAIL = 1
    PHONE = 2
    ORDER_SOURCES = (
        (EMAIL, _('Email')),
        (PHONE, _('Phone')),
    )

    CASH = 1
    CREDIT_CARD = 2
    DEBIT_CARD = 3
    PAYPAL = 4
    PAYMENT_SOURCES = (
        (CASH, _('Cash')),
        (CREDIT_CARD, _('Credit card')),
        (DEBIT_CARD, _('Debit card')),
        (PAYPAL, _('Paypal')),
    )

    COST_CATEGORIES = [3, 7, 15, 35]
    WEIGHT_CHOICES = (
        (COST_CATEGORIES[0], _("less than 5kg")),
        (COST_CATEGORIES[1], _("between 5kg and 15kg")),
        (COST_CATEGORIES[2], _("between 15kg and 25kg")),
        (COST_CATEGORIES[3], _("more than 25kg"))
    )

    source = models.IntegerField(
        _('Source'),
        choices=ORDER_SOURCES,
        null=True,
        blank=True,
    )
    name = models.CharField(
        _('Name'),
        max_length=250,
        null=True,
        blank=True,
    )
    email = models.EmailField(
        _('Email address'),
        max_length=255,
        null=True,
        blank=True,
    )
    phone = models.CharField(
        _("Phone"),
        max_length=200,
        null=True,
        blank=True,
    )
    customer = models.ForeignKey(
        Customer,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_("Customer"),
        related_name="orders",
    )
    item = models.CharField(
        _('Item'),
        max_length=250,
    )
    shop = models.CharField(
        _('Shop'),
        max_length=250,
        null=True,
        blank=True,
        help_text=_("From which shop do you want the item ?")
    )
    address_street = models.CharField(
        _("Your Street"), null=True, max_length=100, blank=True)
    address_house_number = models.CharField(
        _("Your House number"), null=True, blank=True, max_length=20)
    address_extra = models.CharField(
        _("Address more info"), null=True, blank=True, max_length=20)
    address_city = models.CharField(
        _("Your City"), null=True, blank=True, max_length=100)
    address_postal_code = models.CharField(
        _("Your ZIP Code"), null=True, blank=True, max_length=50)
    address_country = models.CharField(
        _("Country"), null=True, blank=True, max_length=100)
    employee = models.ForeignKey(
        Employee,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_("Employee"),
        related_name="deliveries",
    )
    price = models.FloatField(
        _('Approx. price'),
        default=0.0,
    )
    fee = models.FloatField(
        _('Initial Delivery Fee'),
        default=0.0,
    )
    final_fee = models.FloatField(
        _('Final Delivery Fee'),
        default=0.0,
    )
    transgero_charges = models.FloatField(
        _('Employee payment amount'),
        default=0.0,
    )
    display_hashid = models.CharField(
        _('Display HashID'),
        max_length=10,
        null=True,
        blank=True,
    )
    promotion_code = models.ForeignKey(
        Promotion,
        verbose_name=_('Promotion'),
        null=True,
        blank=True,
        on_delete=models.PROTECT,
    )
    feedback = models.TextField(
        _('Feedback'),
        null=True,
        blank=True
    )
    status = models.IntegerField(
        _("Status"),
        default=OPEN,
        choices=STATUS_CHOICES,
    )

    def __str__(self):
        return "{}".format(self.item)

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')

    def save(self, *args, **kwargs):
        """Override the save method to generate the hash id."""
        super(Order, self).save(*args, **kwargs)
        if not self.display_hashid:
            self.display_hashid = encrypt(self.id)
            self.save()

    @property
    def get_total_charge(self):
        if self.final_fee > 0:
            return self.price + self.final_fee
        return self.price + self.fee


class OrderOffer(CreateUpdateModel):

    order = models.ForeignKey(
        Order,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_("Order"),
        related_name="offers",
    )
    employee = models.ForeignKey(
        Employee,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_("Employee"),
        related_name="employee_offers",
    )
    fee = models.FloatField(
        _('Delivery Fee'),
    )
    time_required = models.IntegerField(
        _("Time required (min)"),
    )
    status = models.BooleanField(
        _('Status'),
        default=False,
    )

    def __str__(self):
        return "{} -- {}".format(self.order.item, self.employee.email)

    class Meta:
        verbose_name = _('Offer')
        verbose_name_plural = _('Offers')

    @property
    def encrypted_id(self):
        return encrypt(self.id)