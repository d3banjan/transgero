# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from orders.models import Order, OrderOffer


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):

    def customer_link(self, obj):
        """The admin link of the customer."""
        if obj.customer:
            return "<a href='/admin/accounts/user/{}'>{}</a>".format(
                obj.customer.user.id, obj.customer.user.id)
        return "-"
    customer_link.short_description = _('Customer ID')
    customer_link.allow_tags = True

    def employee_link(self, obj):
        """The admin link of the employee."""
        if obj.employee:
            return "<a href='/admin/accounts/user/{}'>{}</a>".format(
                obj.employee.user.id, obj.employee.user.id)
        return "Not yet assigned"
    employee_link.short_description = _('Employee ID')
    employee_link.allow_tags = True

    list_display = ('created',
                    'source',
                    'status',
                    'name',
                    'customer_link',
                    'email',
                    'phone',
                    'item',
                    'shop',
                    'address_street',
                    'address_house_number',
                    'address_extra',
                    'address_city',
                    'address_postal_code',
                    'address_country',
                    'employee_link',
                    'price',
                    'fee',
                    'final_fee',
                    'transgero_charges',
                    'display_hashid',
                    'promotion_code',
                    'feedback')
    list_filter = ('promotion_code', 'source', 'status', )
    search_fields = ('customer__email', 'display_hashid', 'item')
    ordering = ('-id', )
    list_per_page = 25


@admin.register(OrderOffer)
class OrderOfferAdmin(admin.ModelAdmin):

    def order_link(self, obj):
        """The admin link of the order."""
        if obj.order:
            return "<a href='/admin/orders/order/{}'>{}</a>".format(
                obj.order.id, obj.order.id)
        return "-"
    order_link.short_description = _('Order ID')
    order_link.allow_tags = True

    def employee_link(self, obj):
        """The admin link of the employee."""
        if obj.employee:
            return "<a href='/admin/accounts/user/{}'>{}</a>".format(
                obj.employee.user.id, obj.employee.user.id)
        return "Not yet assigned"
    employee_link.short_description = _('Employee ID')
    employee_link.allow_tags = True

    list_display = ('created',
                    'order_link',
                    'employee_link',
                    'fee',
                    'time_required',
                    'status', )
    search_fields = ('employee__email', 'order__item')
    list_filter = ('status', )
    ordering = ('-id', )
    list_per_page = 25
