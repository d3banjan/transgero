# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0001_initial'),
        ('employees', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(verbose_name='Modified', auto_now=True)),
                ('source', models.IntegerField(null=True, blank=True, verbose_name='Source', choices=[(1, 'Email'), (2, 'Phone')])),
                ('name', models.CharField(max_length=250, null=True, blank=True, verbose_name='Name')),
                ('email', models.EmailField(max_length=255, null=True, blank=True, verbose_name='Email address')),
                ('phone', models.CharField(max_length=200, null=True, blank=True, verbose_name='Phone')),
                ('item', models.CharField(max_length=250, verbose_name='Item')),
                ('shop', models.CharField(help_text='From which shop do you want the item ?', max_length=250, null=True, blank=True, verbose_name='Shop')),
                ('address_street', models.CharField(max_length=100, null=True, blank=True, verbose_name='Your Street')),
                ('address_house_number', models.CharField(max_length=20, null=True, blank=True, verbose_name='Your House number')),
                ('address_extra', models.CharField(max_length=20, null=True, blank=True, verbose_name='Address more info')),
                ('address_city', models.CharField(max_length=100, null=True, blank=True, verbose_name='Your City')),
                ('address_postal_code', models.CharField(max_length=50, null=True, blank=True, verbose_name='Your ZIP Code')),
                ('address_country', models.CharField(max_length=100, null=True, blank=True, verbose_name='Country')),
                ('price', models.FloatField(default=0.0, verbose_name='Approx. price')),
                ('fee', models.FloatField(default=0.0, verbose_name='Initial Delivery Fee')),
                ('final_fee', models.FloatField(default=0.0, verbose_name='Final Delivery Fee')),
                ('transgero_charges', models.FloatField(default=0.0, verbose_name='Employee payment amount')),
                ('display_hashid', models.CharField(max_length=10, null=True, blank=True, verbose_name='Display HashID')),
                ('feedback', models.TextField(null=True, blank=True, verbose_name='Feedback')),
                ('status', models.IntegerField(default=0, verbose_name='Status', choices=[(0, 'Open'), (1, 'Delivery in progress'), (2, 'Delivered'), (3, 'Closed')])),
                ('customer', models.ForeignKey(null=True, verbose_name='Customer', blank=True, to='customers.Customer', on_delete=django.db.models.deletion.SET_NULL, related_name='orders')),
                ('employee', models.ForeignKey(null=True, verbose_name='Employee', blank=True, to='employees.Employee', on_delete=django.db.models.deletion.SET_NULL, related_name='deliveries')),
                ('promotion_code', models.ForeignKey(null=True, verbose_name='Promotion', blank=True, on_delete=django.db.models.deletion.PROTECT, to='customers.Promotion')),
            ],
            options={
                'verbose_name': 'Order',
                'verbose_name_plural': 'Orders',
            },
        ),
        migrations.CreateModel(
            name='OrderOffer',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(verbose_name='Modified', auto_now=True)),
                ('fee', models.FloatField(verbose_name='Delivery Fee')),
                ('time_required', models.IntegerField(verbose_name='Time required (min)')),
                ('status', models.BooleanField(default=False, verbose_name='Status')),
                ('employee', models.ForeignKey(null=True, verbose_name='Employee', blank=True, to='employees.Employee', on_delete=django.db.models.deletion.SET_NULL, related_name='employee_offers')),
                ('order', models.ForeignKey(null=True, verbose_name='Order', blank=True, to='orders.Order', on_delete=django.db.models.deletion.SET_NULL, related_name='offers')),
            ],
            options={
                'verbose_name': 'Offer',
                'verbose_name_plural': 'Offers',
            },
        ),
    ]
