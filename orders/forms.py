# -*- coding: utf-8 -*-
from django import forms
from django.forms import Textarea
from django.core.exceptions import ValidationError
from employees.models import Employee
from orders.models import Order, OrderOffer
from customers.models import Promotion
from simplemathcaptcha.fields import MathCaptchaField
from django.utils.translation import ugettext_lazy as _


class OrderForm(forms.ModelForm):
    """Order form."""
    promotion_code = forms.CharField(
            required=False,
            label=_('Promotion-Code'),
            help_text=_('Do you have any promotion code ?')
    )
    captcha = MathCaptchaField()

    class Meta:
        model = Order
        fields = [
            'captcha',
            'address_city',
            'item',
            'price',
            'fee',
            'phone',
            'email',
            'name',
            'shop',
            'address_street',
            'address_house_number',
            'address_extra',
            'address_postal_code',
            'address_country',
            'promotion_code',
            'source',
        ]

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        self.fields['source'].widget = forms.HiddenInput()
        self.fields['source'].initial = Order.EMAIL
        self.fields['phone'].required = True
        self.fields['email'].required = True
        self.fields['address_city'].required = True
        self.fields['address_street'].required = True
        self.fields['address_house_number'].required = True
        self.fields['address_postal_code'].required = True
        self.fields['address_country'].required = True

    def clean_promotion_code(self):
        promotion_code = self.cleaned_data['promotion_code']
        if not promotion_code:
            return None
        try:
            pc = Promotion.objects.get(code=promotion_code)
            return pc
        except Promotion.DoesNotExist:
            raise ValidationError(_("Promotion code is invalid."))

    def clean_address_city(self):
        address_city = self.cleaned_data['address_city']
        if not address_city:
            raise ValidationError(_("City is required."))
        if Employee.objects.filter(user__address_city=address_city).count() > 0:
            return address_city
        else:
            raise ValidationError(_("No employees in this city."))


class RegisteredCustomerOrderForm(forms.ModelForm):
    """Order form for registered agents."""

    captcha = MathCaptchaField()

    class Meta:
        model = Order
        fields = [
            'captcha',
            'feedback',
        ]
        widgets = {
            'feedback': Textarea(attrs={'cols': 40, 'rows': 3}),
        }


class RegisteredSenderOrderForm(forms.Form):
    """Order form for senders."""

    delivery_status = forms.BooleanField(label=_('Delivery status'),
                                         required=False)


class EmployeeOrderProcessForm(forms.ModelForm):
    """An offer form for an employee for a delivery."""

    class Meta:
        model = OrderOffer
        fields = ['fee', 'time_required', ]