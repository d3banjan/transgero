# -*- coding: utf-8 -*-
from __future__ import absolute_import
# from django_rq import job
from django.conf import settings
from django.utils.translation import ugettext as _
import requests
import json
import time
import sendwithus


def send_to_slack_channel(channel, text):

    try:
        url = settings.SLACK_CHANNELS[channel]
    except KeyError:
        url = settings.SLACK_CHANNELS["default"]

    payload = {
        "text": text
    }

    requests.post(url=url, data=json.dumps(payload))

    return True


# @job('high')
def send_mail_template(sender_name,
                       sender_email,
                       recipient_email,
                       email_data,
                       template_id,
                       tags=None,
                       locale='de-DE'):
    """Send an email from Sendwithus as a task."""

    api = sendwithus.api(api_key=settings.SENDWITHUS_API_KEY)
    r = api.send(
        email_id=template_id,
        recipient={'address': recipient_email},
        email_data=email_data,
        sender={
            'name': sender_name,
            'address': sender_email,
            'reply_to': sender_email,
        },
        # bcc=[{'address': sender_email}],
        tags=tags,
        locale=locale,
    )

    if r.status_code == 200:
        return True
    else:
        raise ValueError(
            _(u'%(status_code)s Error with sendwithus:'
              u' %(content)s ') % {'status_code': r.status_code,
                                   'content': r.content})
