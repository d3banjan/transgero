# -*- coding: utf-8 -*-
import requests
import json
import re
from hashids import Hashids
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def paginated_queryset(queryset, page):
    """Processing queryset based on pagination."""

    from orders.views import ORDERS_PER_PAGE
    paginated_queryset = Paginator(queryset, ORDERS_PER_PAGE)
    try:
        result = paginated_queryset.page(page)
        page = int(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        result = paginated_queryset.page(1)
        page = 1
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        result = paginated_queryset.page(paginated_queryset.num_pages)
        page = paginated_queryset.num_pages
    return result, list(range(1, paginated_queryset.num_pages+1)), page


def split_name(name):
    """Splitting names into first name and last name"""
    if ' ' in name:
        first_name, last_name = str.rsplit(name, ' ', maxsplit=1)
    else:
        first_name = ''
        last_name = name

    return first_name, last_name


def sanitize_string(name):

    return name.strip().lower()


def fix_german_vocals(name):

    return name.replace(u"ü", "ue").replace(u"ä","ae").replace(u"ö","oe").replace(u"ß","ss")


def find_next_id(qset, field, pattern, strip=r'[^0-9]'):
    """
    Find the next free pretty-printed ID.

    Strips away everything which is not a number, increments the resulting
    value by one and passes it through the formatting function/string pattern
    depending on whether pattern is callable or not.
    """
    try:
        obj = qset.order_by('-{}'.format(field))[0]
        latest = int(re.sub(strip, '', getattr(obj, field)))
    except (IndexError, ValueError):
        latest = 0

    if callable(pattern):
        return pattern(latest + 1)

    return pattern % (latest + 1)


def remove_non_alphabets(name):
    name = " ".join(re.findall("[a-zA-Z\sÄÖÜäöüß]+", name))
    return " ".join(name.split())


def encrypt(number):
    try:
        hashids = Hashids(salt='this is Transgero common salt',
                          min_length=6,
                          alphabet="ABCDEFGHIJKLMNPQRSTUVWXYZ123456789")
        return hashids.encode(number)
    except Exception as e:
        print(e)
    return None


def decrypt(number):
    try:
        hashids = Hashids(salt='this is Transgero common salt',
                          min_length=6,
                          alphabet="ABCDEFGHIJKLMNPQRSTUVWXYZ123456789")
        return hashids.decode(number)[0]
    except Exception as e:
        print(e)
    return None


def long_encryption(number):
    try:
        hashids = Hashids(salt='this is Transgero ORDER salt',
                          min_length=20,
                          alphabet="ABCDEFGHIJKLMNPQRSTUVWXYZ123456789")
        return hashids.encode(number)
    except Exception as e:
        print(e)
    return None


def long_decryption(number):
    try:
        hashids = Hashids(salt='this is Transgero ORDER salt',
                          min_length=20,
                          alphabet="ABCDEFGHIJKLMNPQRSTUVWXYZ123456789")
        return hashids.decode(number)[0]
    except Exception as e:
        print(e)
    return None

