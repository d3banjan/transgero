"""
WSGI config for transgero project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
from wsgi_basic_auth import BasicAuth

USERNAME = 'transgero'
PASSWORD = '**transgero**'
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "transgero.settings")


""" ---------------- General settings below ------------------------ """


DISABLED = 0
ENABLED = 1

try:
    PASSWORD_PROTECTED = int(os.environ.get('PASSWORD_PROTECTED', DISABLED))
except Exception as e:
    print(e)
    PASSWORD_PROTECTED = DISABLED

from django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise

if PASSWORD_PROTECTED == ENABLED:
    # Password authentication for development servers
    application = BasicAuth(DjangoWhiteNoise(get_wsgi_application()))
else:
    application = DjangoWhiteNoise(get_wsgi_application())
