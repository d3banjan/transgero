# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='RegistrationProfile',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('activation_key', models.CharField(verbose_name='activation key', max_length=40)),
                ('activated', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name_plural': 'registration profiles',
                'verbose_name': 'registration profile',
            },
        ),
        migrations.CreateModel(
            name='SupervisedRegistrationProfile',
            fields=[
                ('registrationprofile_ptr', models.OneToOneField(serialize=False, primary_key=True, to='registration.RegistrationProfile', parent_link=True, auto_created=True)),
            ],
            bases=('registration.registrationprofile',),
        ),
        migrations.AddField(
            model_name='registrationprofile',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL, verbose_name='user'),
        ),
    ]
