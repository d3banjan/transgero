$(document).ready(function() {

    $('#al_mainmenu a[href^=#]').on('click', function(e){
        var href = $(this).attr('href');
        console.log(href);
        $('html, body').animate({
            scrollTop:$(href).offset().top - 120
        },'slow');
        e.preventDefault();
    });

    $('.parallax-window').parallax({
        speed: 0.6
    });

    $(function(){
        $(".typewriter").typed({
            strings: ["of anything^2500", "to anywhere^2500", "at any time^2500"],
            typeSpeed: 40,
            loop: true
        });
        $(".de-typewriter").typed({
            strings: ["etwas^2500", "irgendwo^2500", "jederzeit^2500"],
            typeSpeed: 40,
            loop: true
        });
    });

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $(function () {
        $(window).bind("load resize", function () {
            var topOffset = 50;
            var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
            if (width < 768) {
                $('div.navbar-collapse').addClass('collapse');
                topOffset = 100; // 2-row-menu
            } else {
                $('div.navbar-collapse').removeClass('collapse');
            }
            var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
            height = height - topOffset;
            if (height < 1) height = 1;
            if (height > topOffset) {
                $("#page-wrapper").css("min-height", (height) + "px");
            }
        });

        var url = window.location;
        var element = $('ul.nav a').filter(function () {
            return this.href == url;
        }).addClass('active').parent();
        while (true) {
            if (element.is('li')) {
                element = element.parent().addClass('in').parent();
            } else {
                break;
            }
        }
    });
});